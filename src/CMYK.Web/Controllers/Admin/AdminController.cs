﻿using CMYK.Web.Models.AdminLogin;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CMYK.Web.Controllers.Admin
{
    public class AdminController : Controller
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;

        public AdminController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public IActionResult Admin()
        {
            if (User.Identity.IsAuthenticated)
            {
                return AdminMenu();
            } 
            else
            {
                return AdminLogin();
            }
        }

        [HttpGet]
        public IActionResult AdminLogin()
        {
            return View(viewName: "Adminlogin");
        }

        [HttpPost]
        public async Task<IActionResult> AdminLogin(LoginModel loginModel)
        {
            var result = await _signInManager.PasswordSignInAsync(loginModel.Email, loginModel.Password, false, false);
            if (result.Succeeded)
            {
                return RedirectToAction("Admin", "Admin");
            }
            else ModelState.AddModelError("", "Неправильный логин и (или) пароль!");
            return View(loginModel);
        }
        
        private IActionResult AdminMenu()
        {
            return View(viewName: "AdminMenu");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AdminLogout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}
