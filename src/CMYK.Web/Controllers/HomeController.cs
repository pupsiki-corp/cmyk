﻿using CMYK.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using CMYK.Core.Repositories;
using CMYK.Infrastructure.Repositories;
using CMYK.Core.Dtos;
using CMYK.Core.Interfaces;
using System.Collections.Generic;
using CMYK.Core.Entities;

namespace CMYK.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IOrderEventService _orderEventService;
        private readonly ILogger<HomeController> _logger;
        private IEventRepository _eventRepository;

        public HomeController(ILogger<HomeController> logger, IEventRepository eventRepository, IOrderEventService orderEventService)
        {
            _orderEventService = orderEventService;
            _logger = logger;
            _eventRepository = eventRepository;
        }

        public IActionResult Index()
        {
            return View(viewName: "Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Events()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpGet]
        public IActionResult OrderEvent()
        {
            return View(viewName: "OrderEvent");
        }
        [HttpPost]
        public IActionResult OrderEvent(AddOrderEventDto dto)
        {
            _orderEventService.AddOrderEvent(dto);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult OrderEventList(string sortType = "", string searchString = "")
        {
            var dtos = _orderEventService.ShowOrderEvents(sortType, searchString);
            List<OrderEvent> model = new List<OrderEvent>();
            foreach (var dto in dtos)
            {
                model.Add(new OrderEvent(dto.Id, dto.Name, dto.Description, dto.Phone, dto.Email));
            }

            return View(viewName: "OrderEventList", model: model);
        }
    }
}