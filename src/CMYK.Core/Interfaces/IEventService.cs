﻿using CMYK.Core.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMYK.Core.Interfaces
{
    public interface IEventService
    {
        void AddEvent(AddEventDto addEventDto);
        void Event(EventDto eventDto);
        void UpdateEvent(UpdateEventDto updateEventDto);
    }
}
