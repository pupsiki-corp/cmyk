﻿using CMYK.Core.Dtos;
using CMYK.Core.Entities;

namespace CMYK.Core.Interfaces
{
    public interface IOrderEventService
    {
        void AddOrderEvent(AddOrderEventDto addOrderEventDto);
        void DeleteOrderEvent(long id);
        void Update(UpdateOrderEventDto updateOrderEventDto);
        OrderEvent? GetById(long id);
        IReadOnlyCollection<OrderEventListDto> ShowOrderEvents(string sortType = "", string searchString = "");
    }
}
