﻿using CMYK.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMYK.Core.Repositories
{
    public interface IOrderEventRepository
    {
        public void AddOrderEvent(OrderEvent orderEvent);
        public void RemoveOrderEvent(long id);
        public void UpdateOrderEvent(OrderEvent orderEvent);
        public List<OrderEvent> GetOrderEvents();
        OrderEvent? GetOrderEventById(long Id);
    }
}
