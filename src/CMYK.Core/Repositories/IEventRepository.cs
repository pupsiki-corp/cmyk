﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMYK.Core.Entities;

namespace CMYK.Core.Repositories
{
    public interface IEventRepository
    {
        public List<Event> GetAll();
        public void AddEvent(Event ev);
        public void RemoveEvent(long id);
        public Event GetById(long id);
    }
}
