﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMYK.Core.Entities
{
    public class Event
    {
        public long Id
        {
            
            set
            {
                if (value < 0) throw new ArgumentException();
                Id = value;
            }
            get { return Id; }
        }

        public string Name
        {
            set
            {
                if (String.IsNullOrEmpty(value)) throw new ArgumentException();
                Name = value;
            }
            get { return Name; }
        }

        public string Description
        {
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException();
                Description = value;
            }
            get { return Description; }
        }

        public string PicturePath
        {
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException();
                PicturePath = value;
            }
            get { return PicturePath; }
        }

        public DateTime EventTime { set; get; }

        public Event()
        {

        }

        public Event(long id, string name, string description, string picturePath, DateTime eventTime)
        {
            if (id < 0 || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description) ||
                string.IsNullOrEmpty(picturePath)) throw new ArgumentException();
            Id = id;
            Name = name;
            Description = description;
            PicturePath = picturePath;
            EventTime = eventTime;
        }

        public Event(Event ev)
        {
            Id = ev.Id;
            Name = ev.Name;
            Description = ev.Description;
            PicturePath = ev.PicturePath;
            EventTime = ev.EventTime;
        }

        public override bool Equals(object? obj)
        {
            if (this == obj)
                return true;

            if (obj == null)
                return false;

            if (obj is not Event item)
                return false;

            return Id == item.Id 
                && Name == item.Name 
                && Description == item.Description 
                && PicturePath == item.PicturePath 
                && EventTime == item.EventTime;
        }

        public override string ToString()
        {
            return $"Event: {{Id={Id}, Name={Name}, Description={Description}, Picture={File.Exists(PicturePath)}, Date={EventTime}}}\n";
        }
    }
}
