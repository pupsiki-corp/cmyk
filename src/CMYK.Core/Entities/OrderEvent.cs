﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMYK.Core.Entities
{
    public class OrderEvent
    {
        private long? _id;
        private string _name;
        private string _description;
        private string _phone;
        private string _email;

        public long? Id
        {
            set
            {
                if (value < 0) throw new ArgumentException();
                _id = value;
            }
            get { return _id; }
        }
        public string Name
        {
            set
            {
                if (String.IsNullOrEmpty(value)) throw new ArgumentException();
                _name = value;
            }
            get { return _name; }
        }
        public string Description
        {
            set
            {
                if (String.IsNullOrEmpty(value)) throw new ArgumentException();
                _description = value;
            }
            get { return _description; }
        }
        public string Phone
        {
            set
            {
                if (String.IsNullOrEmpty(value)) throw new ArgumentException();
                _phone = value;
            }
            get { return _phone; }
        }
        public string Email
        {
            set
            {
                if (String.IsNullOrEmpty(value)) throw new ArgumentException();
                _email = value;
            }
            get { return _email; }
        }
        public OrderEvent(long? id, string Name, string Description, string Phone, string Email)
        {
            this.Id = id;
            this.Name = Name;
            this.Description = Description;
            this.Phone = Phone;
            this.Email = Email;
        }

    }
}
