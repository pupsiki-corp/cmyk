﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMYK.Core.Dtos
{
    public class AddEventDto
    {
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string PicturePath { get; set; } = string.Empty;
        public DateTime EventTime { get; set; }
    }
}
