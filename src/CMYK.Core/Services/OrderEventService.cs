﻿using CMYK.Core.Dtos;
using CMYK.Core.Entities;
using CMYK.Core.Interfaces;
using CMYK.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMYK.Core.Services
{
    public class OrderEventService : IOrderEventService
    {
        private readonly IOrderEventRepository _repository;
        public  OrderEventService(IOrderEventRepository repository)
        {
            _repository = repository;
        }
        public void AddOrderEvent(AddOrderEventDto addOrderEventDto)
        {
            OrderEvent orderEvent = new(null, addOrderEventDto.Name, addOrderEventDto.Description, addOrderEventDto.Phone, addOrderEventDto.Email);
            _repository.AddOrderEvent(orderEvent);
        }

        public void DeleteOrderEvent(long id)
        {
            _repository.RemoveOrderEvent(id);
        }

        public OrderEvent? GetById(long id)
        {
            return _repository.GetOrderEventById(id);
        }

        public IReadOnlyCollection<OrderEventListDto> ShowOrderEvents(string sortType = "", string searchString = "")
        {
            var answer = _repository.GetOrderEvents()
                .Select(ev =>
                {
                    return new OrderEventListDto
                    {
                        Id = (long)ev.Id,
                        Name = ev.Name,
                        Description = ev.Description,
                        Phone = ev.Phone,
                        Email = ev.Email,        
                    };
                })
                .ToList();

            if (!string.IsNullOrEmpty(searchString))
            {
                answer = answer.Where(s => s.Name!.Contains(searchString)).ToList();
            }

            return answer;
        }

        public void Update(UpdateOrderEventDto updateOrderEventDto)
        {
            var orderEvent = new OrderEvent(updateOrderEventDto.Id, updateOrderEventDto.Name, updateOrderEventDto.Description, 
                                            updateOrderEventDto.Phone, updateOrderEventDto.Email);
            _repository.UpdateOrderEvent(orderEvent);
        }
    }
}
