﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMYK.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace CMYK.Infrastructure
{
    public class EventDbContext : DbContext
    {
        private readonly string _connectionString;

        public DbSet<Event> Events { set; get; }

        public DbSet<OrderEvent> OrderEvents { set; get; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql(_connectionString);

        public EventDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }
    }
}
