﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMYK.Core.Entities;
using Microsoft.Extensions.Configuration;
using CMYK.Core.Repositories;

namespace CMYK.Infrastructure.Repositories
{
    public class EfOrderEventRepository : IOrderEventRepository
    {
        private readonly string _connectionString;

        public EfOrderEventRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public void AddOrderEvent(OrderEvent orderEvent)
        {
            using (var db = new EventDbContext(_connectionString))
            {
                db.OrderEvents.Add(orderEvent);
                db.SaveChanges();
            }
        }

        public OrderEvent? GetOrderEventById(long Id)
        {
            using (var db = new EventDbContext(_connectionString))
            {
                return db.OrderEvents.FirstOrDefault(e => e.Id == Id);
            }
        }

        public List<OrderEvent> GetOrderEvents()
        {
            using (var db = new EventDbContext(_connectionString))
            {
                return db.OrderEvents.ToList();
            }
        }

        public void RemoveOrderEvent(long id)
        {
            using (var db = new EventDbContext(_connectionString))
            {
                var orderEvent = db.OrderEvents.FirstOrDefault(e => e.Id == id);
                if (orderEvent == null) return;
                db.OrderEvents.Remove(orderEvent);
                db.SaveChanges();
            }
        }

        public void UpdateOrderEvent(OrderEvent orderEvent)
        {
            using var db = new EventDbContext(_connectionString);
            var result = db.OrderEvents.FirstOrDefault(e => e.Id == orderEvent.Id);
            if (result == null) throw new ArgumentNullException(nameof(result));
            result = orderEvent;
            db.SaveChanges();
        }
    }
}
