﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMYK.Core.Repositories;
using CMYK.Core.Entities;
using Microsoft.Extensions.Configuration;

namespace CMYK.Infrastructure.Repositories
{
    public class EfEventRepository : IEventRepository
    {
        private readonly string _connectionString;

        public EfEventRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }
        public void AddEvent(Event ev)
        {
            using (var db = new EventDbContext(_connectionString))
            {
                db.Events.Add(ev);
                db.SaveChanges();
            }
        }

        public List<Event> GetAll()
        {
            using (var db = new EventDbContext(_connectionString))
            {
                return db.Events.ToList();
            }
        }

        public Event GetById(long id)
        {
            using (var db = new EventDbContext(_connectionString))
            {
                return db.Events.FirstOrDefault(x => x.Id == id);
            }
        }

        public void RemoveEvent(long id)
        {
            int result;
            using (var db = new EventDbContext(_connectionString))
            {
                db.Events.Remove(GetById(id));
                db.SaveChanges();
            }
        }
    }
}
