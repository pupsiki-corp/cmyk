﻿using CMYK.Core.Entities;
using CMYK.Core.Repositories;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMYK.Infrastructure.Repositories
{
    public class SqlOrderEventRepository:IOrderEventRepository
    {
            private string _connectionString;

            public SqlOrderEventRepository(IConfiguration configuration)
            {
                _connectionString = configuration.GetConnectionString("Default");
            }
        public void AddOrderEvent(OrderEvent ev)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            string sql = @$"INTO public.""OrderEvents"" (""Name"", ""Description"", ""Phone"", ""Email"")
            VALUES(@Name, @Description, @Phone, @Email)";

            int rowsAffected = connection.Execute(sql, param: new
            {
                Name = ev.Name,
                Description = ev.Description,
                Phone = ev.Phone,
                Email = ev.Email
            });
        }
        public OrderEvent? GetOrderEventById(long id)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            return connection.QueryFirst<OrderEvent>(@$"SELECT * FROM public.""OrderEvents""
                                                    WHERE Id = {id}");
        }

        public void RemoveOrderEvent(long id)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            int rowsDeleted = connection.Execute(@$"DELETE FROM public.""OrderEvents""
                                                    WHERE Id = {id}");
        }
        public List<OrderEvent> GetOrderEvents()
        {
            using var connection = new NpgsqlConnection(_connectionString);

            var events = connection.Query<OrderEvent>("SELECT * FROM public.\"OrderEvents\"").ToList();

            return events;
        }

        public void UpdateOrderEvent(OrderEvent orderEvent)
        {
            throw new NotImplementedException();
        }

    }
    }