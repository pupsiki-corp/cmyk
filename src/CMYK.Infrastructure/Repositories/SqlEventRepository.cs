﻿using CMYK.Core.Entities;
using CMYK.Core.Repositories;
using Microsoft.Extensions.Configuration;
using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMYK.Infrastructure.Repositories
{
    public class SqlEventRepository : IEventRepository
    {
        private string _connectionString;

        public SqlEventRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public void AddEvent(Event ev)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            string sql = @$"INTO Events (""name"", ""description"", ""picturepath"", ""eventtime"")
            VALUES(@Name, @Description, @PicturePath, @EventTime)";

            connection.Execute(sql, param: new
            {
                Name = ev.Name,
                Description = ev.Description,
                PicturePath = ev.PicturePath,
                EventTime = ev.EventTime
            });
        }

        public List<Event> GetAll()
        {
            using var connection = new NpgsqlConnection(_connectionString);

            var events = connection.Query<Event>("SELECT * FROM Events").ToList();

            return events;
        }

        public Event GetById(long id)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            return connection.QueryFirst<Event>(@$"SELECT * FROM Events
                                                    WHERE Id = {id}");
        }

        public void RemoveEvent(long id)
        {
            using var connection = new NpgsqlConnection(_connectionString);

            connection.Execute(@$"DELETE FROM WHERE Id = {id}");
        }
    }
}
